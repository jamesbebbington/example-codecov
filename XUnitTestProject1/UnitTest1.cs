using ClassLibrary1;
using ClassLibrary2;
using System;
using Xunit;

namespace XUnitTestProject1
{
	public class UnitTest1
	{
		[Fact]
		public void TestA()
		{
			var sut = new FullyTested();
			var result = sut.FullyTested1();
			Assert.Equal(1, result);
		}

		[Fact]
		public void TestB()
		{
			var sut = new FullyTested();
			var result = sut.FullyTested2();
			Assert.Equal(2, result);
		}

		[Fact]
		public void TestC()
		{
			var sut = new PartiallyTested();
			var result = sut.PartiallyTested1();
			Assert.Equal(1, result);
		}
	}
}
